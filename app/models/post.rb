class Post < ActiveRecord::Base
  has_many :comments, dependent: :destroy
  belongs_to :user
  validates_presence_of :title  #filtros, obligamos campo de tipo string.
  validates_presence_of :body   #filtros, obligamos campo de tipo texto.
end
