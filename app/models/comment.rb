class Comment < ActiveRecord::Base
  belongs_to :post
  belongs_to :user
  validates_presence_of :post_id  #filtros, obligamos campo de tipo integer.
  validates_presence_of :body   #filtros, obligamos campo de tipo texto.  
end
